From 8ab916a0fe7b46b20c3a51828600b4f7f207717a Mon Sep 17 00:00:00 2001
From: Vladimir Slavik <vslavik@redhat.com>
Date: Tue, 18 Aug 2020 15:23:49 +0200
Subject: [PATCH] Handle exceptions from threads without new instances

It is not possible to instantiate some exceptions with just an instance as
the only argument, for example UnicodeError and descendants. However, these
days it is possible to raise directly with the provided instance, no need to
instantiate the class. The instance also has the traceback already set, so no
need to set it either.

The original apparently came to be so due to incrementally rewriting python2's
3-argument form of raise. See also previous commits affecting this line,
in chronological order: 07b7034, d16512e, a6085b8.

Resolves: rhbz#1835027
---
 pyanaconda/threading.py | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/pyanaconda/threading.py b/pyanaconda/threading.py
index e0ab80229..d2327cf39 100644
--- a/pyanaconda/threading.py
+++ b/pyanaconda/threading.py
@@ -168,7 +168,7 @@ class ThreadManager(object):
         with self._errors_lock:
             exc_info = self._errors.pop(name)
         if exc_info:
-            raise exc_info[0](exc_info[1]).with_traceback(exc_info[2])
+            raise exc_info[1]
 
     def in_main_thread(self):
         """Return True if it is run in the main thread."""
-- 
2.23.0

